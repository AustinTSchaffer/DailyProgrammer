# Daily Programmer

This is a repository for solutions to problems proposed by either 
<http://www.reddit.com/r/DailyProgrammer> or by some other medium. Most of the 
contained problems are have a small scope, usually only requiring a single 
module as a solution. 

### Organization

The three directories inside the dailyProgrammer directory, labeled "easy", 
"intermediate", and "hard", are the primary categories on the Daily Programmer 
subreddit. Each subdirectory under one of these is labeled by the convention 
"C[challenge#]-[ChallengeName]". 

The directory labelled "foobar" contains challenges that were given to me by 
Google's recruitment beta, "foobar." I completed three of the five challenges, 
but the beta crashed, so I was unable to complete all five. Most of the 
solutions are currently undocumented, but may be at a later date. Some of the 
solutions have postmortems, to evaluate how bad my speed-code was. 

The directory labelled "CodeWars" contains challenges that were completed on 
CodeWars.com. 
